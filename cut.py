#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List

from cvxopt.modeling import variable
from prettytable import PrettyTable

TOTAL = 450
print(f"Общая длина полуфабриката: {TOTAL}")
L = [310, 200, 100]
print(f"Длины заготовок: {L}")
assert min(L) > 0, "Длины заготовок должны быть положительными"
cuts = []  # Способы распила


def cut(i: int, rest: int, c: List[int]):
    """ Генерация способов распила
    :param i: Номер типа заготовки, который сейчас отрезаем
    :param rest: Сколько осталось полуфабриката
    :param c: Текущий разрез
    """
    cur = L[i]  # Длина текущей заготовки
    # Если это последний тип заготовки, то пускаем на неё весь остаток
    if i == len(L) - 1:
        c[i] = rest // cur
        cuts.append(c)  # Добавляем способ разреза
        ct.add_row([ct.rowcount + 1] + c + [rest - c[i] * cur])
        return
    for num in range(rest // cur, -1, -1):
        c[i] = num
        cut(i + 1, rest - num * cur, c)


ct = PrettyTable()
ct.field_names = ["Способ"] + \
                 [chr(i + ord('A')) for i in range(len(L))] + \
                 ["Остаток"]
cut(0, TOTAL, [0] * len(L))
print(ct)

# Количество полуфабриката
SOURCE = 100
# Соотношение для заготовок
PROP = [10, 1, 1]

N = len(cuts)
print(f"Количество способов распила {N}")
# Три переменные: какими способами сколько распилить
x = variable(N, 'x')
k = variable(1, 'k')  # Сколько комплектов получается
#
#kdd = (k * x[0] * cuts[0][0])
print(k)
