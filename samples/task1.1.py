from cvxopt import matrix, solvers
from prettytable import PrettyTable


def outmtrx(matrixs):
    x = PrettyTable()

    x.field_names = ["X", "Значение"]
    c = 0
    for i in matrixs:
        x.add_row(['x' + str(c), '{0:.3f}'.format(i)])
        c += 1

    print(x)


# коэф. перевода стали
Q = 0.7
# матрица стоимости доставки
c = matrix([3.3, 3.3, 3.3, 4.2, 4.2, 4.2, 2.6, 2.6, 2.6, 4.2, 4.2, 4.2], tc='d')
G = matrix([[1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1],
            [0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0],
            [-1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0, 0, 0, -1, 0, 0],
            [0, -1, -Q, 0, 0, 0, 0, -1, -Q, 0, 0, 0],
            [0, 0, 0, 0, -1, -Q, 0, 0, 0, 0, -1, -Q],
            [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1]], tc='d')
# матрица запасов и требований
h = matrix([5300, 3300, 4000, 3300, -1500, -4300, -2100, -4100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], tc='d')
A = matrix([
    [1, 1, Q, 0, 0, 0, 1, 1, Q, 0, 0, 0],
    [0, 0, 0, 1, 1, Q, 0, 0, 0, 1, 1, Q]], tc='d')
# общее потребление
b = matrix([4300, 8800], tc='d')
# получение решения
solution = solvers.lp(c, G.T, h, A.T, b, solver='glpk', options={'glpk': {'msg_lev': 'GLP_MSG_OFF'}})
solution1 = solution

# начало исследования (1)
hSaved = h
while ((solution['x'][2] + solution['x'][5] + solution['x'][8] + solution['x'][11]) < 100):
    # увеличение марки а и уменьшение б
    h[0] = h[0] + 10
    h[1] = h[1] + 10
    if ((h[2] > 10) & (h[3] > 10)):
        h[2] = h[2] - 10
        h[3] = h[3] - 10
    solution = solvers.lp(c, G.T, h, A.T, b, solver='glpk', options={'glpk': {'msg_lev': 'GLP_MSG_OFF'}})
solution2 = solution
h2 = h
h = hSaved

# начало исследования (2)
# создание запасов стали и проверка замен
for number in range(3):
    h[number] = h[number] + 5000
solution = solvers.lp(c, G.T, h, A.T, b, solver='glpk', options={'glpk': {'msg_lev': 'GLP_MSG_OFF'}})
solution3 = solution

st = 0
while ((solution['x'][2] + solution['x'][5] + solution['x'][8] + solution['x'][11]) < 100):
    st += 0.01
    c = matrix([3.3, 3.3 + st, 3.3, 4.2, 4.2 + st, 4.2, 2.6, 2.6 + st, 2.6, 4.2 + st, 4.2, 4.2], tc='d')
    solution = solvers.lp(c, G.T, h, A.T, b, solver='glpk', options={'glpk': {'msg_lev': 'GLP_MSG_OFF'}})
solution4 = solution

print('Задача:')
print('Статус решения: ', solution1['status'], '\n')
outmtrx(solution1['x'])
print('\nИтог: ', solution1['primal objective'])

print('\n\nИсследование 1:\n')
outmtrx(h2)
print('\nСтатус решения: ', solution2['status'], '\n')
outmtrx(solution2['x'])
print('\nПроизведённые замены:\n')
print('a1 -> b1 =', solution2['x'][2], '\na1 -> b2 =', solution2['x'][5], '\na2 -> b1 =', solution2['x'][8],
      '\na2 -> b2 =', solution2['x'][11])
print('\nИтог: ', solution2['primal objective'])

print('\n\nИсследование 2:\n')
print('Статус решения: ', solution3['status'], '\n')
outmtrx(solution3['x'])
print('\nИтог: ', solution3['primal objective'])

print('\nПосле изменения:')
outmtrx(h)
print('\nСтатус решения: ', solution4['status'], '\n')
outmtrx(solution4['x'])
print('\nПроизведённые замены:\n')
print('a1 -> b1 =', solution4['x'][2], '\na1 -> b2 =', solution4['x'][5], '\na2 -> b1 =', solution4['x'][8],
      '\na2 -> b2 =', solution4['x'][11], '\n')
print('Итоговая стоимость перевозки:')
outmtrx(c)
print('\nИтог: ', solution4['primal objective'])
