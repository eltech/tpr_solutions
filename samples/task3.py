#!/usr/bin/env python
# -*- coding: utf-8 -*-
from prettytable import PrettyTable

z = [5, 2, 1, 9, 9, 2]
KEEP = 8  # Стоимость хранения
N = 6  # Количество месяцев
CAPACITY = 15  # Максимальное количество станков
INITIAL_MACHINES = 2  # Начальное количество станков
INCOMING = 7  # Сколько можем привезти за месяц
TRIP = 30  # Стоимость рейса: постоянные затраты
MOVE_COST = 10  # Стоимость доставки одного станка

INF = 10 ** 6  # Заведомо большее число, чем любая стоимость

F = [[INF] * (N + 1) for i in range(CAPACITY + 1)]
F[INITIAL_MACHINES][0] = 0  # Начальное количество станков

for m in range(0, N):
    for i in range(CAPACITY + 1):
        price = F[i][m]
        for k in range(INCOMING + 1):
            machines = i - z[m] + k
            if 0 <= machines <= CAPACITY:
                incoming_costs = (k * MOVE_COST + TRIP if k > 0 else 0)
                keeping_costs = (i + k) * KEEP
                new_price = price + keeping_costs + incoming_costs
                if new_price < F[machines][m + 1]:
                    F[machines][m + 1] = new_price
                    print("Месяц: " + str(m + 1) + " было " + str(i) + " +доставлено " + str(k) +
                          " -забрали " + str(z[m]) + " => " + str(machines) +
                          " на доставку " + str(incoming_costs) + "$ " +
                          " затраты на хранение: " + str(keeping_costs) + "$")

machines = 0
x = PrettyTable()
x.field_names = ["Станки", "0", "1", "2", "3", "4", "5", "Итог"]
for line in F:
    tmp = []
    for f in line:
        tmp.append("%5s" % ("x" if f == INF else f))
    x.add_row(["%2d" % machines, tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6]])
    machines += 1
print(x)
