import sys
sys.stdin = open("C.in", "r")

M = 1000000000
S = int(input())
n = int(input())
for i in range(n):
    S += int(input())
print(0 if S >= M else M - S)
