# import sys
# sys.stdin = open("H.in", "r")

n, m = map(int, input().split())
max_all_sites = 0
for i in range(n):
    min_in_site = min(map(int, input().split()))
    max_all_sites = max(max_all_sites, min_in_site)
print(max_all_sites)
