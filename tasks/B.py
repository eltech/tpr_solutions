import sys

sys.stdin = open("B.in", "r")


def card():
    m, x = input().split()
    return m, int(x)


def sol():
    result = 'YES'
    my = {}  # {'D': 14, 'S': 14}
    for i in range(6):
        m, x = card()
        if not (m in my) or x > my[m]:
            my[m] = x
    for i in range(6):
        m, x = card()
        if not (m in my) or x >= my[m]:
            result = 'NO'
    print(result)


tests = int(input())
for t in range(tests):
    sol()
