import sys

sys.stdin = open("A.in", "r")

N, K = map(int, input().split())
a = []
for i in range(N):
    a.append(int(input()))

left = 0
right = max(a) + 1
while left + 1 < right:
    m = (left + right) // 2
    ropes = sum(x // m for x in a)
    if ropes >= K:
        left = m
    else:
        right = m
print(left)
