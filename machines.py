#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Спрос, шт (по месяцам)
z = [1, 6, 2, 1, 7, 2]
KEEP = 8  # Стоимость хранения
N = 6  # Количество месяцев
CAPACITY = 25  # Максимальное количество станков
INITIAL_MACHINES = 2  # Начальное количество станков
INCOMING = 5  # Сколько можем привезти за месяц
TRIP = 50  # Стоимость рейса: постоянные затраты
MOVE_COST = 10  # Стоимость доставки одного станка

# --------- Стоимость ----------------------------
# Месяц:    |   0  |   1  |   2  | 3 | 4 | 5 | 6 |
# ------------------------------------------------
# Станков 0 | +inf | +inf | +inf |
#         1 | +inf | +inf |
#       ... | 0    | +inf | ...
#        24 | +inf | +inf |
#        25 | +inf | +inf |
# F[i][j] - минимаольная стоимость после i-ого месяца,
#           если после i-ого месяца осталось j станков
#  +inf   - если ситуация невозможна

# "Бесконечность"
INF = 10 ** 6  # Заведомо большее число, чем любая стоимость
# в процессе решения задачи
F = [[INF] * (N + 1) for i in range(CAPACITY + 1)]
F[INITIAL_MACHINES][0] = 0  # Начальное количество станков

for m in range(0, N):
    for i in range(CAPACITY + 1):  # Количество станков
        price = F[i][m]
        for k in range(INCOMING + 1):  # Сколько привозим
            # Получается станков:
            # Сколько было на начало месяца - сколько забрали + сколько привезли
            machines = i - z[m] + k
            if 0 <= machines <= CAPACITY:  # Количество корректно
                # Считаем новую стоимость
                incoming_costs = (k * MOVE_COST + TRIP if k > 0 else 0)
                keeping_costs = (i + k) * KEEP
                new_price = price + keeping_costs + incoming_costs
                if new_price < F[machines][m + 1]:
                    F[machines][m + 1] = new_price
                    print("Месяц: " + str(m + 1) + " было " + str(i) + " +доставлено " + str(k) +
                          " -забрали " + str(z[m]) + " => " + str(machines) +
                          " на доставку " + str(incoming_costs) + "$ " +
                          " затраты на хранение: " + str(keeping_costs) + "$")

machines = 0
for line in F:
    print("%2d" % machines, end=' ')
    machines += 1
    for x in line:
        print(f"%5s" % ("x" if x == INF else x), end=' ')
    print()
