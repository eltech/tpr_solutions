#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Заболеваемость, на 1000 человек
SICKNESS_RATE = [2, 8, 3]
SICKNESS_PROBABILITY = [x / 1000 for x in SICKNESS_RATE]
print(*SICKNESS_PROBABILITY)

# Симптомы заболеваний
P = [
    # C1   C2   C3
    [0.9, 0.6, 0.5],  # З1
    [0.8, 0.8, 0.6],  # З2
    [0.5, 0.8, 0.8],  # З3
]

# Вероятности если есть: C1, C2 и нет C3
PROP = [SICKNESS_PROBABILITY[i] *
        P[i][0] * P[i][1] * (1 - P[i][2])
        for i in range(3)]  # i - заболевание
print(*PROP)

# У пациента не может не быть никаких заболеваний при наличии симптомов
s = sum(PROP)  # Сумма вероятностей
P = [x / s for x in PROP]  # Условные вероятности заболеваний, при условии
# что хотя бы одно заболевание есть
print(*P, sum(P))
