#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Продукт Идентификатор GHGE, кг Популярность Цена, руб. Потребление
# на 100 гр. за 100 гр. в день., гр.
# Картофель 11831 0.15 1 2.9 112
# Хлеб 18060 0.09 1 4.77 90
# Яйца 01129 0.27 0.95 74.7 120
# Фрукты 09003 0.07 0.8 8.1 100
# Печенье 18204 0.14 0.8 30 53
# Злаки 08144 0.12 0.75 15 0
# Овощи 11894 0.16 0.78 30 143
# Масло 01001 0.22 0.8 106 20
# Сыр 01018 1.19 0.75 56 154
# Молоко 01291 0.13 0.85 7.2 378
# Орехи 12135 0.21 0.65 125 15
# Рыба 15237 0.77 0.8 61 0
# Мясо 13317 0.65 0.9 53 317

# Текущий рацион
cur = [112, 90, 120, 100, 53, 0, 143, 20, 154, 378, 15, 0, 317]
price = [2.9, 4.77, 74.7, 8.1, 30, 15, 30, 106, 56, 7.2, 125, 61, 53]
GHGE = [0.15, 0.09, 0.27, 0.07, 0.14, 0.12, 0.16, 0.22, 1.19, 0.13, 0.21, 0.77, 0.65]


# Белков

# Жиров

# Углеводов

def sum_price(cur):
    sum = 0  # Текущая стоимость рациона
    for i in range(len(cur)):
        # Цена указана для 100 гр. и средний месяц считаем 30 дней
        sum += 30 * cur[i] * price[i] / 100.0
    return sum


def sum_GHGE(cur):
    cur_GHGE = 0  # Количество выбросов
    for i in range(len(cur)):
        # GHGE, кг на 100 гр.
        cur_GHGE += cur[i] * GHGE[i] / 100.0
    return cur_GHGE


print(sum_price(cur), sum_GHGE(cur))
