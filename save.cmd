SET HOUR=%time:~0,2%
SET dtStamp9=%date:~-4%%date:~4,2%%date:~7,2%_0%time:~1,1%%time:~3,2%%time:~6,2% 
SET dtStamp24=%date:~7,2%.%date:~4,2%.%date:~-4% %time:~0,2%:%time:~3,2%
if "%HOUR:~0,1%" == " " (SET dtStamp=%dtStamp9%) else (SET dtStamp=%dtStamp24%)

git add .
git commit -a -m"Save %dtStamp%"
git pull --all
git push origin